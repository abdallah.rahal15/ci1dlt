package fr.levitt.tp4.tp4web.core;

public class DisplayInformation {

    private String direction;

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }
}
