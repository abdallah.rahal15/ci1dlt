package fr.levitt.tp4.tp4web.cache;

public class NoOPCache<T> implements CustomCache<T> {

    @Override
    public T getCache() {
        return null;
    }

    @Override
    public void updateCache(T cache) {
        System.out.println("Warning : cache inactif");
    }

    @Override
    public void invalidate() {
        System.out.println("Warning : cache inactif");
    }
}
